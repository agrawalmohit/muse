# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for
from mongoengine.queryset import DoesNotExist

from app.mod_track.models import Track, Genre
import jsonify
from flask import render_template
from app import db
from bson import json_util
import json
import pdb
import datetime
import pdb



# Define the blueprint
# This module provides methods to perform Creation, Read, Update and Delete of Tracks and Genres
mod_track = Blueprint('track', __name__, url_prefix='/')


def respond(msg, status=200):
	return json.dumps({
	    "status" : status,
		"data" : msg
	})

def error(errmsg, status=400):
    return json.dumps({
	    "status" : status,
		"error" : errmsg
	})

def parse(original):
	if type(original) is dict:
		new = dict((k.encode('ascii'), parse(v)) for (k, v) in original.items())
	elif isinstance(original, unicode):
		new = original.encode('ascii')
	else :
		new = original
	return new



@mod_track.route('', methods=['GET'])
def index():
	return render_template('index.html')


@mod_track.route('track', methods=['GET'])
def tracks():
	tracks = Track.list()
	return json.dumps(tracks)



@mod_track.route('track/add', methods=['POST'])
def add_track():
	"""Adds new tracks to the database
    Args:
        track (dict): Dict with track data (title, genres, rating)
    Returns:
        json: The return value. Object saved in the databse with _id
    """
	data = request.get_json()
	try:
		new_track = Track.add(data['track'])
	except Exception as e:
		return json.dumps({'error' : str(e)}), 500
	
	return json.dumps(new_track), 200


@mod_track.route('track/update', methods=['POST'])
def update_track():
	"""Updates tracks to the database
    Args:
        track (dict): Dict with updated track data (title, genres, rating)
    Returns:
        None
    """
	data = request.get_json()
	try:
		Track.update(**data['track'])
	except Exception as e:
		return json.dumps({'error' : str(e)}), 500
	
	return "", 200


@mod_track.route('track/rate', methods=['POST'])
def rate_track():
	"""Updates the rating of the tracks
    Args:
        ratingData (dict): Dict with track rating data (_id, rating)
    Returns:
        None
    """
	data = request.get_json()
	try:
		Track.update(**data)
	except Exception as e:
		return json.dumps({'error' : str(e)}), 500
	
	return "", 200


@mod_track.route('track/delete', methods=['POST'])
def delete_track():
	"""DEletes existing track from the database
    Args:
        trackId (str): Id of the track to be deleted
    Returns:
        None
    """
	data = request.get_json()
	try:
		Track.objects(id=data['trackId']).delete()
	except Exception as e:
		return json.dumps({'error' : str(e)}), 500
	
	return "", 200




@mod_track.route('genre', methods=['GET'])
def genre():
	genres = Genre.list()
	return genres

 
@mod_track.route('genre', methods=['POST'])
def add_genre():
	"""Adds new genres to the database
    Args:
        name (str): Name of the genre
    Returns:
        None
    """
	data = request.get_json()
	try:
		Genre.add(data['name'])
	except Exception as e:
		return jsonify({'message' : str(e)}), 500

	return  "",200
