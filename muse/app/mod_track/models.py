from app import db

from mongoengine import *
from mongoengine.fields import StringField
from mongoengine.fields import DateTimeField
from datetime import datetime
from bson import ObjectId

import pdb
import json


class Track(Document):
	title = StringField()
	genres = ListField()
	rating = FloatField()

	def add(trackData):
		try:
			doc = Track(**trackData).save()
			track_obj = parse_doc_to_obj(doc)
		except Exception as e:
			raise Exception(e)
		return track_obj


	def list():
		records = []
		try:
			docs = Track.objects()
			for doc in docs:
				track_obj = parse_doc_to_obj(doc)
				records.append(track_obj)
		except DoesNotExist:
			records = []
		except Exception as e:
			raise Exception(e)
		return records

	def update(**kwargs):
		track =  Track.objects(id=ObjectId(kwargs['_id']))
		kwargs.pop('_id', None)
		update_status = track.update(**kwargs)
		return update_status # 0 or 1

class Genre(Document):
	names = ListField()

	def add(genreName):
		try:
			genre_doc = Genre.objects().get()
		except DoesNotExist:
			genre_doc = Genre(names=[]).save()
		
		genres = genre_doc.to_mongo().to_dict()['names']
		if (genreName not in genres):
			genres.append(genreName)
			genre_doc.update(names=genres)

	def list():
		try:
			genres = Genre.objects().get().to_mongo().to_dict()['names']
		except DoesNotExist:
			genres = []
		return json.dumps(genres)



def parse_doc_to_obj(doc):
	obj = doc.to_mongo().to_dict()
	obj.update({'_id' : str(obj['_id'])})
	return obj
