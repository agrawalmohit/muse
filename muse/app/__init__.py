import os

# Import flask and template operators
from flask import Flask, render_template
from flask_mongoengine import MongoEngine
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_wtf import CsrfProtect

# import pytz
# from pytz import utc
from datetime import datetime


import csv
import logging
import datetime
import config

# Define the WSGI application object
app = Flask(__name__ , static_url_path='/static')

app.config.from_object(config.DevelopmentConfig)
db = MongoEngine(app)


bcrypt = Bcrypt(app)
app.debug = True
#CsrfProtect(app)
# Import a module / component using its blueprint handler variable (mod_user)

from app.mod_track.controllers       import mod_track          as track_module


# Register blueprint(s)
app.register_blueprint(track_module)


# Exception Handling

from app.mod_track.exceptions import DatabaseException


@app.errorhandler(DatabaseException)
def handle_database_exception(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404









