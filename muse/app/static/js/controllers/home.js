app.controller('HomeCtrl', function($scope, TrackService, initialData) {

	$scope.genres = initialData.genres;
	$scope.tracks = initialData.tracks;

	$scope.addGenre = function() {
		TrackService.addGenre($scope.newGenre)
		.then(function(){
			$scope.genres.push($scope.newGenre)
			$scope.newGenre = null
		})
	}

	$scope.addTrack = function(){
		TrackService.addTrack($scope.newTrack)
		.then(function(newTrack){
			$scope.tracks.push(newTrack.data);
			$scope.newTrack = {}
		})
	}

	$scope.editTrack = function(track){
		$scope.editing = true;
		$scope.backup = angular.copy(track)
		$scope.editingTrack = track;
	}

	$scope.update = function(){
		$scope.editing = false;
		TrackService.updateTrack($scope.editingTrack)
		.error(function(err){
			$scope.tracks[$scope.tracks.indexOf($scope.editingTrack )] = angular.copy($scope.backup)
		})

	}

	$scope.cancelEdit = function(){
		$scope.editing = false;

	}

	$scope.deleteTrack = function(track){
		TrackService.deleteTrack(track._id)
		.then(function(){
			$scope.tracks.splice($scope.tracks.indexOf(track), 1)
		})
	}
	$scope.updateRating = function(trackData){
		console.log(trackData)
		TrackService.updateRating({'_id' : trackData._id, 'rating' : trackData.rating})
	}


})

