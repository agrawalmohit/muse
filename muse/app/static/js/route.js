 app.config( function ( $stateProvider, $urlRouterProvider ) {
   $urlRouterProvider.otherwise('/');

    $stateProvider
        
        // STATES AND VIEWS ========================================
        .state('home', {
            url: '/',
            templateUrl: 'static/views/home.html',
            controller : 'HomeCtrl',
            resolve: {
                initialData : ["TrackService", "$q", function(TrackService, $q) {
                    return $q.all({
                        'tracks' : TrackService.getTracks(),
                        'genres' : TrackService.getGenres()
                    })
                }]
            }  
        })

});

