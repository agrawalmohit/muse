
app.factory('TrackService' , function($http){

	var service = {
		addTrack : function(trackData){
			return $http.post('/track/add', {'track' : trackData})
			.error(function(response){
				console.log(response.error)
			})
		},
		getTracks : function(track){
			return $http.get('/track')
			.then(function(response){
				return response.data
			})
		},
		updateTrack : function(trackData){
			return $http.post('/track/update', {'track' : trackData})
		},
		deleteTrack : function(trackId){
			return $http.post('/track/delete', {'trackId' : trackId})
			.error(function(response){
				console.log(response.error)
			})
		},
		updateRating : function(ratingData){
			return $http.post('/track/rate', ratingData)
			.error(function(response){
				console.log(response.error)
			})
		},
		addGenre : function(genreName){
			return $http.post('/genre', {'name' : genreName})
			.error(function(response){
				console.log(response)
			})
		},
		getGenres : function(){
			return $http.get('/genre')
			.then(function(response){
				return response.data
			})
		}

	}

	return service

})
