from flask_script import Manager, Server


from flask import Flask
from flask_mongoengine import MongoEngine

import os, sys
import config
from config import BASE_DIR
from app import app, db
import json
import pdb


if BASE_DIR not in sys.path:
	sys.path.append(BASE_DIR)


manager = Manager(app)

manager.add_command('runserver', Server(
		use_debugger = True,
		use_reloader=True,
		host='0.0.0.0',
		port=9090
	))

if __name__ == '__main__':
    manager.run()
    print("Server running at localhost:9000")

    