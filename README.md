# Muse

### What is this repository for? ###

* This is an web application to help collect and find your favorite music across all genres 

### Tech Stack ###
* The Web API is made in Python using Flask framework which is a lightweight framework suitable for creating Web services <br/>
* MongoDB is used for database as it is a NoSQL, scalable database solution <br/>
* The client interface is a single page application made using Angular JS <br/>

### Setup ###

* Create and activate virtual environment with python 3 <br/>
    $ virtualenv -p python3 venv <br/>
    $ source venv/bin/activate


* Install and run MongoDB on port 27017

* Install dependencies from the requirements.txt file <br/>
    $ pip install -r requirements.txt


* Run the application <br/>
    $ python manage.py runserver

* Goto localhost:9090 to interact with the UI 
